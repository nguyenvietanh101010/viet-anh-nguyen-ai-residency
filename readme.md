# FPT Software AI Residency 
## Additional Project Invitation


### My name is Viet Anh Nguyen, 4th-year student-oriented  Artificial Intelligence at Hanoi University of Science and Technology (ĐHBKHN) (I'm in a 5 year engineering program)



This Github contains primarily two main folders:

1) Codes and trained models:

This folder contains the versions of the notebook which I've trained. Due to the lack of GPU, I managed to train my customized CNN models and the first idea of SegFormer's encoder based carefully (but Segformer's results aren't good).

My customized Densenet and the second ideas aren't fine-tuned. That's why the results are quite bad

2) Data:

This folder contains my drawing of proposed ideas as well as the paper directly related to the FGVC dataset



Note: The dataset is already on FGVC-Aircraft (https://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/)




Again, thank you for giving me this chance to participate in solving this interesting problem and trying to conduct a shortened version of publication!