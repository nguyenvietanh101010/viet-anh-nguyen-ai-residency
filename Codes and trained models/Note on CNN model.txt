Because I haven't load the best model of the CNN model to predict on the test set, 
the accuracy on the test set metric from the notebook is just the last version of the model, not the best one.


If loaded with the best model (already provided), the accuracy on the test set is much better!